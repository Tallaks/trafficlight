﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightController : MonoBehaviour
{
    [SerializeField] private PedestrianTrafficLight _pedestrianTrafficLigthRignt;
    [SerializeField] private PedestrianTrafficLight _pedestrianTrafficLigthLeft;
    [SerializeField] private AutoTransportTrafficLight _transportTrafficLigthRight;

    private List<TrafficLight> _allTrafficLights;
    private float _elapsedMoveTimeSeconds = 10;
    private float _elapsedStopTimeSeconds = 10;
    private float _greenLightFlickerTime = 3;
    private float _yellowLightTime = 1f;

    void Start()
    {
        _allTrafficLights = new List<TrafficLight>() 
        {
            _pedestrianTrafficLigthRignt,
            _pedestrianTrafficLigthLeft,
            _transportTrafficLigthRight 
        };

        TurnOnAllTrafficLights();
        StartCoroutine(ControlByTimer());
    }

    public void TurnOnAllTrafficLights()
    {
        foreach (var trafficLight in _allTrafficLights)
        {
            trafficLight.TurnOn();
        }

        StartCoroutine(ControlByTimer());
    }

    public void TurnOffAllTrafficLights()
    {
        foreach (var trafficLight in _allTrafficLights)
        {
            trafficLight.TurnOff();
        }

        StopAllCoroutines();
    }

    private IEnumerator ControlByTimer()
    {
        while (true)
        {
            AllowToMoveOneWay();
            yield return new WaitForSeconds(_elapsedMoveTimeSeconds);

            InformThatMovePermissionIsEnding();
            yield return new WaitForSeconds(_greenLightFlickerTime);

            InformThatMovePermissionHasEnded();
            yield return new WaitForSeconds(_yellowLightTime);

            AllowToMoveOtherWay();
            yield return new WaitForSeconds(_elapsedStopTimeSeconds);

            InformThatMovePermissionIsComing();
            yield return new WaitForSeconds(_yellowLightTime);
        }
    }

    private void AllowToMoveOneWay()
    {
        _pedestrianTrafficLigthRignt.HandleMoveSignal();
        _pedestrianTrafficLigthLeft.HandleStopSignal();
        _transportTrafficLigthRight.HandleMoveSignal();
    }

    private void InformThatMovePermissionIsEnding()
    {
        _transportTrafficLigthRight.HandlePrepareForStop(_greenLightFlickerTime);
    }

    private void InformThatMovePermissionHasEnded()
    {
        _transportTrafficLigthRight.HandlePrepareForRedLight();
    }

    private void InformThatMovePermissionIsComing()
    {
        _transportTrafficLigthRight.HandlePrepareForMove();
    }

    private void AllowToMoveOtherWay()
    {
        _pedestrianTrafficLigthRignt.HandleStopSignal();
        _pedestrianTrafficLigthLeft.HandleMoveSignal();
        _transportTrafficLigthRight.HandleStopSignal();
    }

    public void ChangeGreenTransportLight(float newTime)
    {
        _elapsedMoveTimeSeconds = newTime;
    }

    public void ChangeRedTransportLight(float newTime)
    {
        _elapsedStopTimeSeconds = newTime;
    }
}
