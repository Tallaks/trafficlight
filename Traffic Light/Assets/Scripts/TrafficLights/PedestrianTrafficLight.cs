﻿using UnityEngine;

public class PedestrianTrafficLight : TrafficLight
{
    [SerializeField] private GameObject _greenLight;
    [SerializeField] private GameObject _redLight;

    public override void HandleMoveSignal()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Проезд разрешен");
            _redLight.SetActive(false);
            _greenLight.SetActive(true);
        }
    }

    public override void HandleStopSignal()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Проезд запрещен");
            _redLight.SetActive(true);
            _greenLight.SetActive(false);
        }
    }

    public override void TurnOff()
    {
        base.TurnOff();

        _redLight.SetActive(false);
        _greenLight.SetActive(false);
    }
}
