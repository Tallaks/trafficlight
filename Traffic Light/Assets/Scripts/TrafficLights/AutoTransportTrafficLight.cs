﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTransportTrafficLight : TrafficLight
{
    [SerializeField] private GameObject _greenLight;
    [SerializeField] private GameObject _yellowLight;
    [SerializeField] private GameObject _redLight;

    public override void HandleMoveSignal()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Проезд разрешен");
            _redLight.SetActive(false);
            _yellowLight.SetActive(false);
            _greenLight.SetActive(true);
        }
    }

    public override void HandleStopSignal()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Проезд запрещен");
            _redLight.SetActive(true);
            _yellowLight.SetActive(false);
            _greenLight.SetActive(false);
        }
    }

    public void HandlePrepareForMove()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Приготовиться к движению");
            _redLight.SetActive(true);
            _yellowLight.SetActive(true);
            _greenLight.SetActive(false);
        }
    }

    public void HandlePrepareForRedLight()
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Сейчас загорится красный");
            _redLight.SetActive(false);
            _yellowLight.SetActive(true);
            _greenLight.SetActive(false);
        }
    }

    public void HandlePrepareForStop(float flickerTime)
    {
        if (_isWorking)
        {
            _infoPanel.SetSignalStete("Сейчас загорится желтый");
            _redLight.SetActive(false);
            _yellowLight.SetActive(false);

            StartCoroutine(FlickerGreenLight(flickerTime));
        }
    }

    private IEnumerator FlickerGreenLight(float flickerTime)
    {
        for(int i = 0; i < 3; i++)
        {
            _greenLight.SetActive(true);
            yield return new WaitForSeconds(flickerTime / 6);

            _greenLight.SetActive(false);
            yield return new WaitForSeconds(flickerTime / 6);
        }
    }

    public override void TurnOff()
    {
        base.TurnOff();

        StopAllCoroutines();

        _redLight.SetActive(false);
        _greenLight.SetActive(false);
        _yellowLight.SetActive(false);
    }
}
