﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TrafficLight : MonoBehaviour
{
    [SerializeField] protected InfoOfTrafficLight _infoPanel;

    protected bool _isWorking = true;

    public abstract void HandleMoveSignal();
    public abstract void HandleStopSignal();
    public virtual void TurnOff()
    {
        _isWorking = false;
        _infoPanel.SetStateLabel(_isWorking);
        _infoPanel.SetSignalStete("Светофор недоступен");
    }

    public virtual void TurnOn() 
    {
        _isWorking = true;
        _infoPanel.SetStateLabel(_isWorking);
    }
}
