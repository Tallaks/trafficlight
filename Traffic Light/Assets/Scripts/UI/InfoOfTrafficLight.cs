﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoOfTrafficLight : MonoBehaviour
{
    [SerializeField] private Text _stateText;
    [SerializeField] private Text _signalText;

    public void SetStateLabel(bool state)
    {
        _stateText.text = state ? "Включен" : "Отключен";
    }

    public void SetSignalStete(string signalDescription)
    {
        _signalText.text = signalDescription;
    }
}
