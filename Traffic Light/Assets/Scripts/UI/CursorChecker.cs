﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorChecker : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
    private ExtendedFlycam _cameraMover;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _cameraMover.IsWithinUI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _cameraMover.IsWithinUI = false;
    }

    private void Start()
    {
        _cameraMover = Camera.main.GetComponent<ExtendedFlycam>();
    }  
}
