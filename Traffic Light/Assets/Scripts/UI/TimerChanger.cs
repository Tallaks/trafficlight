﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerChanger : MonoBehaviour
{
    [SerializeField] private Slider _greenLightSLider;
    [SerializeField] private Slider _redLightSLider;
    [SerializeField] private Text _greenLightSLiderValue;
    [SerializeField] private Text _redLightSLiderValue;
    [SerializeField] private TrafficLightController _trafficLightController;

    // Start is called before the first frame update
    void Start()
    {
        _greenLightSLiderValue.text = _greenLightSLider.value.ToString() + " сек.";
        _redLightSLiderValue.text = _redLightSLider.value.ToString() + " сек.";

        _greenLightSLider.onValueChanged.AddListener((float value) =>
        {
            _trafficLightController.ChangeGreenTransportLight(value);
            _greenLightSLiderValue.text = value.ToString() + " сек.";
        });

        _redLightSLider.onValueChanged.AddListener((float value) =>
        {
            _trafficLightController.ChangeRedTransportLight(value);
            _redLightSLiderValue.text = value.ToString() + " сек.";
        });
    }
}
