﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class TurnOnOffButton : MonoBehaviour
{
    private Text _buttonText;
    private Button _button;
    private bool _nowIsTurnedOn;
    private TrafficLightController _trafficLightController;

    void Start()
    {
        _trafficLightController = FindObjectOfType<TrafficLightController>();

        _nowIsTurnedOn = true;

        _button = GetComponent<Button>();
        _buttonText = GetComponentInChildren<Text>();
        _buttonText.text = "Отключить все светофоры";

        _button.onClick.AddListener(() => SwitchOnOffState());
    }

    private void SwitchOnOffState()
    {
        _nowIsTurnedOn = !_nowIsTurnedOn;

        _buttonText.text = _nowIsTurnedOn ? "Отключить все светофоры" : "Включить все светофоры";

        if (_nowIsTurnedOn)
        {
            _trafficLightController.TurnOnAllTrafficLights();
        }
        else
        {
            _trafficLightController.TurnOffAllTrafficLights();
        }
    }
}
