# Тестовое задание
## Описание:
Написать контроллер, какого угодно вида, управляющий светофором. Светофор, это дорожный регулировщик с лампочками, работающий на любых дорогах, например, железных или автомобильных и, для регулировки движения, использующий только световую сигнализацию (без подвижных, например, частей). Можно считать, что контроллер работает с ограничениями: например, поддерживает максимальное число сигналов, равное пяти.
## Задачи:
 - Ответить на вопрос: какие проблемы ставит задача?
 - Ответить на вопрос: как возможно решить соответствующие проблемы?
 - Реализовать решение в виде приложения с использованием Unity3D
## Требования к реализации:
Подготовить ассет для визуализации светофора произвольной конфигурации. Подключить реализованный контроллер к ассету, таким образом, чтобы было возможно визуально оценить работу системы после запуска проекта в редакторе. Для ассета светофора, в ситуации с использованием элементов пользовательского интерфейса: верстка должна выглядеть пропорционально подобно на максимально бОльшем количестве устройств (для максимального числа разрешений экрана) Допускается использование ассета с трехмерной моделью. В составе ассета должно присутствовать сообщение в текстовом виде, видимое после компиляции, поясняющее текущий сигнал светофора. В пояснении необходимо указать название настроенной для оценки сцены. Проект решения передать через любой удобный Git репозиторий.

# Ответы на вопросы
## Какие проблемы ставит задача?
Основная проблема - контроллер, отправляющий сигналы, не должен знать, как именно светофор будет эти сигналы обрабатывать, какие лампочки загорятся, это не его задача. Контроллер должен лишь распределять сигналы по их адресатам при соблюдении необходимых условий. 

Кроме того, в моей реализации использовалась стандартная работа светофоров по таймеру, а не по некому триггеру. Соответственно, нужно было создать повторяющийся цикл, во время которого последовательно отправляются различные сигналы. Необходмио было реализовать это наименее затратно.

## Как возможно решить соответствующие проблемы?
Первая проблема решается созданием базового класса светофора с логикой, общей для всех светофоров, и созданием классов-наследников с прописанным механизмом реакции на тот или иной сигнал. Соответственно, контроллер отправляет сигналы, не зная, сколько и какие лампочки у того, кто этот сигнал получит.

Вторая проблема решается созданием бесконечного цикла из корутин. Это лучше, чем подача сигналов каждый фрейм внутри функции Update. В случае с корутинами каждое действие выполняется лишь тогда, когда это требуется.

## Настроенная для оценки сцена
\Traffic Light\Assets\Scenes\TestScene.unity